----------------------------------------------------------------------------------------------------
-- SeasonsLivestockTrailer
----------------------------------------------------------------------------------------------------
-- Purpose:  Removes animals when the night passes
--
-- Copyright (c) Realismus Modding, 2019
----------------------------------------------------------------------------------------------------

SeasonsLivestockTrailer = {}

function SeasonsLivestockTrailer.prerequisitesPresent(specializations)
    return SpecializationUtil.hasSpecialization(LivestockTrailer, specializations)
end

function SeasonsLivestockTrailer.registerOverwrittenFunctions(vehicleType)
    SpecializationUtil.registerOverwrittenFunction(vehicleType, "dayChanged", SeasonsLivestockTrailer.dayChanged)
    SpecializationUtil.registerOverwrittenFunction(vehicleType, "addAnimal", SeasonsLivestockTrailer.addAnimal)
    SpecializationUtil.registerOverwrittenFunction(vehicleType, "removeAnimal", SeasonsLivestockTrailer.removeAnimal)
end

function SeasonsLivestockTrailer.registerEventListeners(vehicleType)
    SpecializationUtil.registerEventListener(vehicleType, "onLoad", SeasonsLivestockTrailer)
end

function SeasonsLivestockTrailer:onLoad(savegame)
    local spec = self.spec_livestockTrailer

    spec.animalHealth = 0

    if savegame ~= nil and not savegame.resetVehicles then
        local key = self:seasons_getSpecSaveKey(savegame.key, "seasonsLivestockTrailer")
        spec.animalHealth = Utils.getNoNil(getXMLFloat(savegame.xmlFile, key .. "#animalHealth"), spec.animalHealth)
    end
end

function SeasonsLivestockTrailer:onWriteStream(streamId, connection)
    local spec = self.spec_livestockTrailer
    streamWriteFloat32(streamId, spec.animalHealth)
end

function SeasonsLivestockTrailer:onReadStream(streamId, connection)
    local spec = self.spec_livestockTrailer
    spec.animalHealth = streamReadFloat32(streamId)
end

function SeasonsLivestockTrailer:saveToXMLFile(xmlFile, key, usedModNames)
    local spec = self.spec_livestockTrailer
    setXMLFloat(xmlFile, key .. "#animalHealth", spec.animalHealth)
end

function SeasonsLivestockTrailer:dayChanged(superFunc)
    local spec = self.spec_livestockTrailer

    superFunc(self)

    local num = #spec.loadedAnimals

    for i = #spec.loadedAnimals, 1, -1 do
        self:removeAnimal(spec.loadedAnimals[i])
    end

    if num > 0 then
        g_currentMission:addIngameNotification(FSBaseMission.INGAME_NOTIFICATION_CRITICAL, string.format(g_i18n:getText("seasons_notification_animalsDiedInTrailer"), num))
    end
end

function SeasonsLivestockTrailer:addAnimal(superFunc, animal)
    local spec = self.spec_livestockTrailer
    local husbandryHealth = spec.loadingTrigger.husbandry.averageGlobalProductionFactor

    if #spec.loadedAnimals == 0 then
        spec.animalHealth = husbandryHealth
    else
        spec.animalHealth = (spec.animalHealth * num + husbandryHealth) / (num + 1)
    end

    superFunc(self, animal)
end

function SeasonsLivestockTrailer:removeAnimal(superFunc, animal)
    local spec = self.spec_livestockTrailer

    superFunc(self, animal)

    if #spec.loadedAnimals == 0 then
        spec.animalHealth = 0
    end
end